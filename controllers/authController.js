const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const {User, Refresh_token} = require('../models')

const secretKey = 'secretkeyjwt'

module.exports = {
    registerPage:(req, res) =>{
        res.render('register')
    },

    registerUser:(req, res) =>{
        const {email, password} = req.body
        //enkripsi password
        const encryptedPassword = bcrypt.hashSync(password, 10)
        //save to db
         User.create({
            email:email,
            password:encryptedPassword,
            createdAt: new Date(),
            updatedAt: new Date()
        })
        .then(resp => {
            return res.json({json:200, message:'register sucess'})
        }).catch(err => {
            res.json({
                status:403,
                message:'failed insert data ',err
            })
        })

    },

    loginUser: async (req, res) => {
        const { email, password} = req.body

        const userx = await User.findOne({
            where: {
                email:email
            }
        })
        if (!userx) {
            return res.json({ status:400, message:'User not found'}).status(400)
        }
        const isValidPassword = bcrypt.compareSync(password, userx.password)
        if (!isValidPassword) {
            return res.json({status:400, message:'wrong password'}).status(400)
        }

        const accessToken = jwt.sign({
            id:userx.id,
            email:userx.email,
            type:'access_token'
        }, secretKey, {
            expiresIn:'1m'
        })

        const refreshToken = jwt.sign({
            id:userx.id,
            email:userx.email,
            type:'refresh_token'
        }, secretKey, {
            expiresIn:'5m'
        })  
        
        await Refresh_token.create({
            user_id:userx.id,
            token:refreshToken,
            createdAt:new Date(),
            updatedAt:new Date()
        })

        const response = {
            id:userx.id,
            email:userx.email,
            access_token:accessToken,
            refresh_token:refreshToken
        }
        res.json({status:200, message:'Successfull', response:response})
    },

    refreshToken:async(req, res) => {
        const {token} = req.body
        const refreshToken = await Refresh_token.findOne({
            where:{
                token:token
            }
        })

        if (!refreshToken) {
            return res.json({status:400, message:'token not defined'}).status(400)
        }

        const userx = await User.findOne({
            where:{
                id:refreshToken.dataValues.user_id
            }
        })

        if (!userx) {
            return res.json({status:400, message:'user not found'}).status(400)
        }
        const accessToken = jwt.sign({
            id:userx.id,
            email:userx.email,
            type:'access_token'
        }, secretKey, {
            expiresIn:'1m'
        })

        const reToken = jwt.sign({
            id:userx.id,
            email:userx.email,
            type:'refresh_token'
        }, secretKey, {
            expiresIn:'5m'
        })  
        
        await Refresh_token.create({
            user_id:userx.id,
            token:reToken,
            createdAt:new Date(),
            updatedAt:new Date()
        })

        const response = {
            id:userx.id,
            email:userx.email,
            access_token:accessToken,
            refresh_token:reToken
        }
        res.json({status:200, message:'Successfull', response:response})
    }
}
