const {product} = require('../models')


module.exports = {
    home:(req, res) => {
        const {id, username} = req.user
        res.json({message:'successfull with username '+username+' and id '+ id})
    },

    add_item: async (req, res) => {
        product.findAll({
            raw : true ,
            nest: true ,
            where:{
                nama_product:req.body.nama_product
            }
        })
        .then(resp => {
            if (resp.length == 0) {
                
                product.create({
                    nama_product:req.body.nama_product,
                })
                .then(resp => {
                    res.json({
                        status:201,
                        message:'success insert data',
                        data:[
                           resp
                        ]
                    })
                })
                .catch(err => {
                    res.json({
                        status:403,
                        message:'failed insert data ',err
                    })
                })
            }else{
                res.status(400)
                res.json({
                    message:'failed, username was taken',
                })
            }
        })
        .catch(err => {
            res.status(500)
            res.json({
                message:err,
            })
        })
    },

    show_item: async (req, res) => {
        product.findAll({
            raw : true ,
            nest: true 
        })
        .then(resp => {
            res.json({
                status:200,
                message:'success retrive data',
                data:[
                   resp
                ]
            })
        })
        .catch(err => {
            res.status(500)
            res.json({
                message:err.message,
            })
        })
    },

    update_item: async (req, res) => {
        product.update(
            {
                nama_product:req.body.nama_product,
            },
            {
            where: {
                id: req.params.id
            },
            }
        )
            .then(function (resp) {
                res.status(201)
                res.json({
                    message:'success'
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    
    },

    delete_item: async (req, res) => {
        product.destroy({
            where:{
                id:req.params.id
            }
        })
        .then(resp => {
            res.status(200)
            res.json({
                message:'success'
            })
        })
        .catch(err => {
            res.status(500)
            res.json({
                message:err.message
            })
        })
    
    }
    
}