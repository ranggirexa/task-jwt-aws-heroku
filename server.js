const express = require('express')

const router = require('./router.js')
const port = process.env.PORT || 7200

const app = express()

const swaggerUI = require('swagger-ui-express')
const swaggerJson = require('./docs/swagger.json')

app.use('/api/v0/docs', swaggerUI.serve, swaggerUI.setup(swaggerJson))

app.use(express.urlencoded({extended:false}))
app.use(express.json())

app.set('view engine', 'ejs')

app.use(router)

app.listen(port, ()=>{
    console.log(`server runing in port ${port}`);
})
