const router = require('express').Router()
const authController = require('./controllers/authController')
const auth = require('./middlewares/auth')
const homeController = require('./controllers/homeController')


// router.get('/register', authController.registerPage)
router.post('/register', authController.registerUser)
// router.get('/login', authController.loginPage)
router.post('/login', authController.loginUser)
router.post('/refresh-token', authController.refreshToken)

router.get('/home', auth, homeController.home)

router.post("/add_item" ,auth ,homeController.add_item)
router.get("/items" ,auth ,homeController.show_item)
router.put("/update_item/:id" ,auth ,homeController.update_item)
router.delete("/delete_item/:id" ,auth ,homeController.delete_item)


module.exports = router
