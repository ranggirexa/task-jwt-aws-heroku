const jwt = require("jsonwebtoken")

module.exports = (req, res, next) => {
    try {
        const token = req.header("Authorization")
        if (!token) return res.status(403).json({message:'"access denied.'})
        
        const decode = jwt.verify(token,'secretkeyjwt')
        console.log(decode);
        if (decode.type == 'refresh_token') {
            return res.status(403).json({message:'"invalid token'})
        }
        req.user = decode
        next()
    } catch (error) {
        res.status(400).json({message:'token expired'})
    }
}
